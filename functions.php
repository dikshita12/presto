<?php
add_action('wp_enqueue_scripts', 'presto_removeScripts' , 20);
function presto_removeScripts() {
//De-Queuing paresnt color Styles sheet 
wp_dequeue_style( 'default',get_template_directory_uri() .'/css/default.css'); 

//EN-Queing new color Style sheet

 $parent_style = 'parent-style';
 wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
 wp_enqueue_style('purple', get_stylesheet_directory_uri() . '/purple.css');  
}?>
<?php function presto_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'after_setup_theme', 'presto_add_editor_styles' );