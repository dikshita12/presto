=== Presto ===
Contributors: weblizar
Tags: two-columns, three-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready , blog  , custom-logo , E-Commerce , footer-widgets , portfolio 
Requires at least: 4.0
Tested up to: 5.5
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Presto is a free WordPress child theme, brings plenty of customization possibilities.

== Description ==
Description: Presto is a child theme of most famous WordPress theme Enigma. It is mordern and easy to use. The theme is full of the latest features. Presto is the perfect theme for the professionals, bloggers, and creative personnel’s website, as it provides a clean and flexible appearance, an elegant portfolio, and a catchy online shop. The theme is translation ready, fully SEO optimized, fast loading and is fully compatible with woo commerce and all other major WordPress page builder plugins like Elementor, Visual Composer , SiteOrigin, Beaver Builder, Divi ,Page Builder Sandwich.

<a href="https://wordpress.org/themes/presto/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/presto"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

== Changelog ==

----1.2-----
* Update according to latest enigma .
* Screenshot update

----1.0.1-----
***Minor Fixes.

----1.0-----
*** Enigma V6.0.1 & above compatible 

= 0.9 =
*** BS4 Competible.

= 0.8 =
* added new page full width template

= 0.7 =
* added new post template(left sidebar)

= 0.6 =
* Hire Us Page added.
* WP-5.0.X Version compatible.
* Readme file updated as per new rule.

= 0.5 =
* New template "Product Template" added
* Search option added
* Box layout issue fixed
* Screenshot updated

= 0.4 =
* New template "Template Home-2" added.

== Resources ==

== Image Resources ==
= Screenshot =
* https://pxhere.com/en/photo/5311 | CC0 Public Domain
All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)

Parent Theme License:
Enigma WordPress Theme, Copyright 2018 by weblizar
Enigma is distributed under the terms of the GNU GPL

Child Them License
Presto WordPress Child Theme, Copyright 2018 by  weblizar
Enigma is distributed under the terms of the GNU GPL