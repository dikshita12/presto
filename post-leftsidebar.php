<?php
//Template Name: Left sidebar layout
//Template Post Type: post
get_header();
$class = '';
if ( get_theme_mod( 'breadcrumb', 1 ) == 1 ) {
	get_template_part( 'breadcrums' ); 
} else { 
	$class = 'no-breadcrumb';
} 
?>
<div class="container">	
	<div class="row enigma_blog_wrapper <?php echo esc_attr( $class ); ?>">
	<?php get_sidebar(); ?>
	<div class="col-md-8">	
		<?php 
		if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			get_template_part('post','content'); 
		endwhile; 
		else : 
			get_template_part('nocontent');
		endif;
		comments_template( '', true ); 
		?>
	</div>	
	</div> <!-- row div end here -->	
</div><!-- container div end here -->
<?php get_footer(); ?>